﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DemoApplication.Domain.Database;
using DemoApplication.Domain.Database.DatabaseModel;
using DemoApplication.Domain.Database.Repository;
using DemoApplication.Domain.Model;
using Moq;
using Xunit;

namespace DemoApplication.Domain.Tests
{
    public class ProductRepositoryTest
    {
        private readonly Mock<IDbContextFactory> _dbContextFactoryMock = new Mock<IDbContextFactory>();
        private readonly Mock<EposDbContext> _dbContext = new Mock<EposDbContext>();

        public ProductRepositoryTest()
        {
            SetupMock();
        }

        private void SetupMock()
        {
            var products = new List<Product>()
            {
                new Product
                {
                    Name = "1",
                    Id = 1,
                    ServerId = 1,
                    Barcode = "1",
                    PriceUnit = 1,
                    PriceCold = 1,
                    CategoryId = 1,
                    LastEdit = DateTime.Now,
                    Enable = true
                },
                new Product
                {
                    Name = "12",
                    Id = 2,
                    ServerId = 1,
                    Barcode = "12",
                    PriceUnit = 1,
                    PriceCold = 1,
                    CategoryId = 1,
                    LastEdit = DateTime.Now,
                    Enable = true
                },
                new Product
                {
                    Name = "123",
                    Id = 3,
                    ServerId = 1,
                    Barcode = "123",
                    PriceUnit = 1,
                    PriceCold = 1,
                    CategoryId = 1,
                    LastEdit = DateTime.Now,
                    Enable = true
                }
            }.AsQueryable();
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "1",
                    Enable = true
                },
                new Category
                {
                    Id = 2,
                    Name = "2",
                    Enable = true
                }
            }.AsQueryable();
            var suppliers = new List<Supplier>
            {
             new Supplier
             {
                 Name = "1",
                 Id = 1,
                 ServerId = 1,
                 Enable = true
             },
             new Supplier
             {
                 Name = "2",
                 Id = 2,
                 ServerId = 2,
                 Enable = true
             }
            }.AsQueryable();
            var mockProduct = new Mock<DbSet<Product>>();
            mockProduct.As<IQueryable<Product>>().Setup(m => m.Provider).Returns(products.Provider);
            mockProduct.As<IQueryable<Product>>().Setup(m => m.Expression).Returns(products.Expression);
            mockProduct.As<IQueryable<Product>>().Setup(m => m.ElementType).Returns(products.ElementType);
            mockProduct.As<IQueryable<Product>>().Setup(m => m.GetEnumerator()).Returns(products.GetEnumerator());

            var mockCategory = new Mock<DbSet<Category>>();
            mockCategory.As<IQueryable<Category>>().Setup(m => m.Provider).Returns(categories.Provider);
            mockCategory.As<IQueryable<Category>>().Setup(m => m.Expression).Returns(categories.Expression);
            mockCategory.As<IQueryable<Category>>().Setup(m => m.ElementType).Returns(categories.ElementType);
            mockCategory.As<IQueryable<Category>>().Setup(m => m.GetEnumerator()).Returns(categories.GetEnumerator());

            var mockSupplier = new Mock<DbSet<Supplier>>();
            mockSupplier.As<IQueryable<Supplier>>().Setup(m => m.Provider).Returns(suppliers.Provider);
            mockSupplier.As<IQueryable<Supplier>>().Setup(m => m.Expression).Returns(suppliers.Expression);
            mockSupplier.As<IQueryable<Supplier>>().Setup(m => m.ElementType).Returns(suppliers.ElementType);
            mockSupplier.As<IQueryable<Supplier>>().Setup(m => m.GetEnumerator()).Returns(suppliers.GetEnumerator());

            _dbContext.Setup(d => d.Products).Returns(mockProduct.Object);
            _dbContext.Setup(d => d.Categories).Returns(mockCategory.Object);
            _dbContext.Setup(d => d.Suppliers).Returns(mockSupplier.Object);
            _dbContextFactoryMock.Setup(x => x.NewDbContext()).Returns(_dbContext.Object);
        }

        [Fact]
        public async void TestGetAllCategory()
        {
            var fixture = new ProductRepository(_dbContextFactoryMock.Object);
            var categories = await fixture.GetAllCategory();
            Assert.Equal(2, categories.Count);
        }

        [Fact]
        public async void TestGetAllSupplier()
        {
            var fixture = new ProductRepository(_dbContextFactoryMock.Object);
            var suppliers = await fixture.GetAllSupplier();
            Assert.Equal(2, suppliers.Count);
        }
        [Fact]
        public async void TestSearchProductWithEmptyFilterThenReturnAllProduct()
        {
            var fixture = new ProductRepository(_dbContextFactoryMock.Object);
            var searchModel = new ProductSearchModel();
            var products = await fixture.SearchProduct(searchModel);
            Assert.Equal(3, products.Count);
        }
        [Fact]
        public async void TestSearchProductWithFilterThenReturnProductMatchCritical()
        {
            var fixture = new ProductRepository(_dbContextFactoryMock.Object);
            var searchModel = new ProductSearchModel
            {
                Barcode = "2"
            };
            var products = await fixture.SearchProduct(searchModel);
            Assert.Equal(2, products.Count);
        }
    }
}
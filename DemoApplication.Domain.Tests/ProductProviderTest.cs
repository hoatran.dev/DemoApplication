﻿using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using DemoApplication.Domain.Database.Repository;
using DemoApplication.Domain.Model;
using DemoApplication.Domain.Provider;
using Moq;
using Xunit;

namespace DemoApplication.Domain.Tests
{
    public class ProductProviderTest
    {
        private readonly Mock<IProductRepository> _productRepositoryMock = new Mock<IProductRepository>();

        public ProductProviderTest()
        {
            SetupMock();
        }

        private void SetupMock()
        {
            _productRepositoryMock.Setup(x => x.GetAllCategory()).Returns(Task.FromResult(new List<CategoryDto>
            {
                new CategoryDto
                {
                    Id = 1,
                    Name = "Category 1"
                },
                new CategoryDto
                {
                    Id = 2,
                    Name = "Category 2"
                }
            }));
            _productRepositoryMock.Setup(x => x.GetAllSupplier()).Returns(Task.FromResult(new List<SupplierDto>
            {
                new SupplierDto
                {
                    Id = 1,
                    Name = "Supplier 1",
                    ServerId = 1
                },
                new SupplierDto
                {
                    Id = 2,
                    Name = "Supplier 2",
                    ServerId = 2
                },
                new SupplierDto
                {
                    Id = 3,
                    Name = "Supplier 3",
                    ServerId = 3
                }
            }));
            _productRepositoryMock.Setup(x => x.SearchProduct(It.IsAny<ProductSearchModel>(),It.IsAny<int>())).Returns(Task.FromResult(new List<ProductDto>
            {
                new ProductDto
                {
                    Name = "Product 1",
                    Id = 1,
                    Barcode = "Barcode 1",
                    Selling = 1,
                    Cost = 1
                },
                new ProductDto
                {
                    Name = "Product 2",
                    Id = 2,
                    Barcode = "Barcode 2",
                    Selling = 2,
                    Cost = 2
                },
                new ProductDto
                {
                    Name = "Product 3",
                    Id = 3,
                    Barcode = "Barcode 3",
                    Selling = 3,
                    Cost = 3
                }
            }));
        }

        [Fact]
        public async void TestGetAllCategory()
        {
            var fixture = new ProductProvider(_productRepositoryMock.Object);
            var categories = await fixture.GetAllCategory().SingleAsync();
            Assert.Equal(2, categories.Count);
        }
        [Fact]
        public async void TestGetAllSupplier()
        {
            var fixture = new ProductProvider(_productRepositoryMock.Object);
            var suppliers = await fixture.GetAllSupplier().SingleAsync();
            Assert.Equal(3, suppliers.Count);
        }
        [Fact]
        public async void TestSearchProduct()
        {
            var fixture = new ProductProvider(_productRepositoryMock.Object);
            var products = await fixture.SearchProduct(new ProductSearchModel()).SingleAsync();
            Assert.Equal(3, products.Count);
        }
    }
}
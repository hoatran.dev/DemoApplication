﻿using System.Reactive.Linq;
using DemoApplication.Domain.Model;
using DemoApplication.ViewModel.Main;
using Moq;
using ReactiveUI;
using Xunit;

namespace DemoApplication.Tests
{
    public class MainViewModelTest
    {
        private readonly Mock<IScreen> _screenMock = new Mock<IScreen>();
        public MainViewModelTest() { }

        [Fact]
        public async void TestWhenSearchResultContainNormalProductThenOrderListShouldBeUpdated()
        {
            var fixture = new MainViewModel(_screenMock.Object);
            fixture.SearchProductResult.RegisterHandler(i => i.SetOutput(new ProductDto
            {
                Id = 1,
                Name = "Product 1",
                Barcode = "Barcode 1",
                Cost = 1,
                Selling = 1,
                ProfitMargin = ""
            }));
            await fixture.Search.Execute();
            Assert.Equal(1, fixture.Orders.Count);
        }

        [Fact]
        public async void TestWhenSearchResultContainEmptyProductThenOrderListDoNotChange()
        {
            var fixture = new MainViewModel(_screenMock.Object);
            fixture.SearchProductResult.RegisterHandler(i => i.SetOutput(new ProductDto()));
            await fixture.Search.Execute();
            Assert.Equal(0, fixture.Orders.Count);
        }
    }
}
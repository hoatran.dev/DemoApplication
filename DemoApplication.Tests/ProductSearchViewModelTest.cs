﻿using System.Collections.Generic;
using System.Reactive.Linq;
using DemoApplication.Domain.Model;
using DemoApplication.Service;
using DemoApplication.ViewModel.Product.Dialog;
using Moq;
using Xunit;

namespace DemoApplication.Tests
{
    public class ProductSearchViewModelTest
    {
        private readonly Mock<IProductService>  _productServiceMock = new Mock<IProductService>();

        public ProductSearchViewModelTest()
        {
            SetupMock();
        }

        void SetupMock()
        {
            _productServiceMock.Setup(p => p.GetCategory()).Returns(Observable.Return(new List<CategoryDto>
            {
                new CategoryDto
                {
                    Id = 1,
                    Name = "Category 1"
                },
                new CategoryDto
                {
                    Id = 2,
                    Name = "Category 2"
                }
            }));
            _productServiceMock.Setup(p => p.GetSupplier()).Returns(Observable.Return(new List<SupplierDto>
            {
                new SupplierDto
                {
                    Id = 1,
                    Name = "Supplier 1",
                    ServerId = 1
                },
                new SupplierDto
                {
                    Id = 2,
                    Name = "Supplier 2",
                    ServerId = 2
                },
                new SupplierDto
                {
                    Id = 3,
                    Name = "Supplier 3",
                    ServerId = 3
                }
            }));
            _productServiceMock.Setup(p => p.SearchProduct(It.IsAny<ProductSearchInfoViewModel>()))
                .Returns(Observable.Return(new List<ProductDto>
                {
                    new ProductDto
                    {
                        Name = "Product 1",
                        Id = 1,
                        Barcode = "Barcode 1",
                        Selling = 1,
                        Cost = 1
                    },
                    new ProductDto
                    {
                        Name = "Product 2",
                        Id = 2,
                        Barcode = "Barcode 2",
                        Selling = 2,
                        Cost = 2
                    },
                    new ProductDto
                    {
                        Name = "Product 3",
                        Id = 3,
                        Barcode = "Barcode 3",
                        Selling = 3,
                        Cost = 3
                    }
                }));
        }

        [Fact]
        public async void TestSearchProductThenProductListShouldBeUpdated()
        {
            var fixture = new ProductSearchViewModel(_productServiceMock.Object);
            Assert.Equal(0, fixture.Product.Count);
            await fixture.SearchProduct.Execute(fixture.SearchModel);
            Assert.Equal(3, fixture.Product.Count);
        }

        [Fact]
        public async void TestLoadSearchDataThenCategoryShouldBeLoaded()
        {
            var fixture = new ProductSearchViewModel(_productServiceMock.Object);
            Assert.Equal(0, fixture.Categories.Count);
            await fixture.LoadSearchData.Execute();
            //The Category list contain the empty category after loaded
            Assert.Equal(3, fixture.Categories.Count);
        }

        [Fact]
        public async void TestLoadSearchDataThenSupplierShouldBeLoaded()
        {
            var fixture = new ProductSearchViewModel(_productServiceMock.Object);
            Assert.Equal(0, fixture.Suppliers.Count);
            await fixture.LoadSearchData.Execute();
            //The Suppliers list contain the empty supplier after loaded
            Assert.Equal(4, fixture.Suppliers.Count);
        }
    }
}
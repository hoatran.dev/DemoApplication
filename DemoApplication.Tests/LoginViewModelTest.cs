﻿using System.Linq;
using DemoApplication.ViewModel.Login;
using Moq;
using ReactiveUI;
using Xunit;

namespace DemoApplication.Tests
{
    public class LoginViewModelTest
    {
        private readonly Mock<IScreen> _screenMock = new Mock<IScreen>();
        

        public LoginViewModelTest()
        {
            SetupMock();
        }

        private void SetupMock()
        {
            _screenMock.SetupGet(s => s.Router).Returns(new RoutingState());
        }

        [Fact]
        public void TestInputInvalidPinCodeThenPinCodeAutoClear()
        {
            var fixture = new LoginViewModel(_screenMock.Object);
            fixture.PinCode = "3456";
            Assert.Equal(fixture.PinCode,string.Empty);
        }

        [Fact]
        public void TestInputValidPinCodeThenAutoNavigateToMainView()
        {
            var fixture = new LoginViewModel(_screenMock.Object);
            fixture.PinCode = "1234";
            var currentViewName = fixture.HostScreen.Router.NavigationStack.LastOrDefault()?.UrlPathSegment ??
                                  string.Empty;
            Assert.Equal("Main",currentViewName);
        }
    }
}
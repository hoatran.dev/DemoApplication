﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using DemoApplication.Domain.Model;
using ReactiveUI;

namespace DemoApplication.ViewModel.Main
{
    public class MainViewModel : ReactiveObject, IRoutableViewModel
    {
        public ReactiveCommand<Unit, Unit> Search { get; }
        private readonly Interaction<Unit, ProductDto> _searchProduct;
        public Interaction<Unit, ProductDto> SearchProductResult => _searchProduct;
        private readonly ReactiveList<OrderModel> _orders = new ReactiveList<OrderModel>();
        public IReadOnlyReactiveList<OrderModel> Orders => _orders;
        public string UrlPathSegment
        {
            get => "Main";
        }
        public IScreen HostScreen { get; }

        public MainViewModel(IScreen screen)
        {
            HostScreen = screen;
            Search = ReactiveCommand.CreateFromObservable<Unit, Unit>(_ => SearchImp());
            _searchProduct = new Interaction<Unit, ProductDto>();
        }

        public IObservable<Unit> SearchImp()
        {
            return Observable.Start(() =>
            {
                _searchProduct.Handle(Unit.Default).SubscribeOn(RxApp.MainThreadScheduler).Subscribe(p =>
                {
                    if (!string.IsNullOrEmpty(p?.Barcode))
                    {
                        _orders.Add(new OrderModel(p));
                    }
                });
            });
        }
    }
}
﻿using System;
using DemoApplication.Domain.Model;
using ReactiveUI;

namespace DemoApplication.ViewModel.Main
{
    public class OrderModel
    {
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }

        public OrderModel(ProductDto productDto)
        {
            ProductName = productDto.Name;
            Quantity = 1;
            Price = productDto.Selling;
            Amount = Quantity * Price;
        }
    }
}
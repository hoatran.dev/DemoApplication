﻿using DemoApplication.ViewModel.Login;
using DemoApplication.ViewModel.Main;
using ReactiveUI;

namespace DemoApplication.ViewModel
{
    public class ShellViewModel : ReactiveObject, IRoutableViewModel
    {
        public string UrlPathSegment
        {
            get => "Shell";
        }
        public IScreen HostScreen { get; }
        public LoginViewModel LoginViewModel;

        public ShellViewModel(IScreen screen)
        {
            HostScreen = screen;
            LoginViewModel = new LoginViewModel(HostScreen);
            HostScreen.Router.Navigate.Execute(LoginViewModel);
        }
    }
}
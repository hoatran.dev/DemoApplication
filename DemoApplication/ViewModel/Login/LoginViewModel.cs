﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using DemoApplication.ViewModel.Main;
using ReactiveUI;

namespace DemoApplication.ViewModel.Login
{
    public class LoginViewModel : ReactiveObject, IRoutableViewModel
    {
        private string _pinCode = string.Empty;

        public string PinCode
        {
            get => _pinCode;
            set => this.RaiseAndSetIfChanged(ref _pinCode, value);
        }

        public ReactiveCommand<int, Unit> UserInput;
        public string UrlPathSegment
        {
            get => "Login";
        }
        public IScreen HostScreen { get; protected set; }

        public LoginViewModel(IScreen screen)
        {
            HostScreen = screen;
            UserInput = ReactiveCommand.CreateFromObservable<int, Unit>(UserInputImp);
            this.WhenAnyValue(x => x.PinCode).Subscribe(input =>
            {
                if (!string.IsNullOrEmpty(input) && input.Length == 4)
                {
                    if ("1234".Equals(input))
                    {
                        HostScreen.Router.Navigate.Execute(new MainViewModel(HostScreen));
                    }
                    else
                    {
                        PinCode = string.Empty;
                    }
                }
            });
        }

        public IObservable<Unit> UserInputImp(int input)
        {
            return Observable.Start(() =>
            {
                if (input == -1)
                {
                    PinCode = string.Empty;
                }
                else if (_pinCode.Length < 4)
                {
                    PinCode += input;
                }
            }, RxApp.MainThreadScheduler);
        }
    }
}
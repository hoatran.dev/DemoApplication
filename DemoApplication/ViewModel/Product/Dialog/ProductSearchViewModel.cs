﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using DemoApplication.Domain.Model;
using DemoApplication.Service;
using ReactiveUI;

namespace DemoApplication.ViewModel.Product.Dialog
{
    public class ProductSearchViewModel : ReactiveObject
    {
        public ProductSearchInfoViewModel SearchModel = new ProductSearchInfoViewModel();
        public ReactiveCommand<ProductSearchInfoViewModel, List<ProductDto>> SearchProduct { get; }
        public ReactiveCommand<Unit, Unit> CancelSearch { get; }
        public ReactiveCommand<Unit, Unit> SelectProduct { get; }
        public ReactiveCommand<Unit, Unit> LoadSearchData { get; }

        private readonly Interaction<ProductDto, Unit> _searchResult;
        public Interaction<ProductDto, Unit> SearchProductResult => _searchResult;
        private readonly ReactiveList<ProductDto> _product = new ReactiveList<ProductDto>();

        public IReadOnlyReactiveList<ProductDto> Product => _product;

        private readonly ReactiveList<CategoryDto> _categories = new ReactiveList<CategoryDto>();

        public IReadOnlyReactiveList<CategoryDto> Categories => _categories;

        private readonly ReactiveList<SupplierDto> _suppliers = new ReactiveList<SupplierDto>();

        public IReadOnlyReactiveList<SupplierDto> Suppliers => _suppliers;

        public ProductDto SelectedProduct;
        public IProductService ProductService;
        private bool _loadingIndicator = true;

        public bool LoadingIndicator
        {
            get => _loadingIndicator;
            set => this.RaiseAndSetIfChanged(ref _loadingIndicator, value);
        }
        public ProductSearchViewModel(IProductService productService)
        {
            ProductService = productService;
            _searchResult = new Interaction<ProductDto, Unit>();
            SearchProduct =
                ReactiveCommand.CreateFromObservable<ProductSearchInfoViewModel, List<ProductDto>>(SearchProductImp);
            SearchProduct.ObserveOn(RxApp.MainThreadScheduler).Subscribe(items =>
            {
                using (_product.SuppressChangeNotifications())
                {
                    _product.Clear();
                    _product.AddRange(items);
                }
            });
            CancelSearch = ReactiveCommand.CreateFromObservable<Unit, Unit>(x => CancelSearchImp());
            SelectProduct = ReactiveCommand.CreateFromObservable<Unit, Unit>(x => SelectProductImp());
            LoadSearchData = ReactiveCommand.CreateFromObservable<Unit, Unit>(x => LoadSearchDataImp());
        }

        public IObservable<List<ProductDto>> SearchProductImp(ProductSearchInfoViewModel searchModel)
        {
            return ProductService.SearchProduct(searchModel);
        }

        public IObservable<Unit> CancelSearchImp()
        {
            return Observable.Start(() =>
            {
                _searchResult.Handle(new ProductDto()).Subscribe();
            });
        }

        public IObservable<Unit> SelectProductImp()
        {
            return Observable.Start(() =>
            {
                _searchResult.Handle(SelectedProduct).Subscribe();
            });
        }

        public IObservable<Unit> LoadSearchDataImp()
        {
            return Observable.Start(() =>
            {
                ProductService.GetCategory().ObserveOn(RxApp.MainThreadScheduler).Zip(ProductService.GetSupplier().ObserveOn(RxApp.MainThreadScheduler), (category, supplier) =>
                {
                    using (_categories.SuppressChangeNotifications())
                    {
                        category.Insert(0, new CategoryDto { Id = -1, Name = string.Empty });
                        _categories.Clear();
                        _categories.AddRange(category);
                    }
                    using (_suppliers.SuppressChangeNotifications())
                    {
                        supplier.Insert(0, new SupplierDto { Id = -1, Name = string.Empty, ServerId = -1 });
                        _suppliers.Clear();
                        _suppliers.AddRange(supplier);
                    }
                    return true;
                }).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x =>
                {
                    LoadingIndicator = false;
                });
            });
        }
    }
}
﻿using ReactiveUI;

namespace DemoApplication.ViewModel.Product.Dialog
{
    public class ProductSearchInfoViewModel : ReactiveObject
    {

        private string _barcode;

        public string Barcode
        {
            get => _barcode;
            set => this.RaiseAndSetIfChanged(ref _barcode, value);
        }

        private string _supplierCode;

        public string SupplierCode
        {
            get => _supplierCode;
            set => this.RaiseAndSetIfChanged(ref _supplierCode, value);
        }

        private string _name;

        public string Name
        {
            get => _name;
            set => this.RaiseAndSetIfChanged(ref _name, value);
        }

        private int _supplier;

        public int Supplier
        {
            get => _supplier;
            set => this.RaiseAndSetIfChanged(ref _supplier, value);
        }

        private int _category;

        public int Category
        {
            get => _category;
            set => this.RaiseAndSetIfChanged(ref _category, value);
        }
    }
}
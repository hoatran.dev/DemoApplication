﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows;
using DemoApplication.Domain.Model;
using DemoApplication.ViewModel.Product.Dialog;
using ReactiveUI;
using ReactiveUI.Legacy;

namespace DemoApplication.View.Product.Dialog
{
    /// <summary>
    /// Interaction logic for ProductSearchView.xaml
    /// </summary>
    public partial class ProductSearchView : IViewFor<ProductSearchViewModel>
    {
        public ProductDto SelectedProduct;
        public ProductSearchView(ProductSearchViewModel productSearchViewModel)
        {
            InitializeComponent();
            ViewModel = productSearchViewModel;
            this.WhenActivated(d =>
            {
                d(ViewModel.SearchProductResult.RegisterHandler(handle =>
                {
                    handle.SetOutput(Unit.Default);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        SelectedProduct = handle.Input;
                        DialogResult = handle.Input != null;
                    });
                }));
                d(this.Bind(ViewModel, x => x.SearchModel.Barcode, x => x.Barcode.Text));
                d(this.Bind(ViewModel, x => x.SearchModel.SupplierCode, x => x.SupplierCode.Text));
                d(this.Bind(ViewModel, x => x.SearchModel.Name, x => x.Name.Text));
                d(this.Bind(ViewModel, x => x.SearchModel.Supplier, x => x.Supplier.SelectedValue));
                d(this.Bind(ViewModel, x => x.SearchModel.Category, x => x.Category.SelectedValue));
                d(this.Bind(ViewModel, x => x.LoadingIndicator, x => x.LoadingIndicator.Visibility,vmToViewConverterOverride: new BooleanToVisibilityTypeConverter()));
                d(this.OneWayBind(ViewModel, x => x.Product, x => x.Products.ItemsSource));
                d(this.OneWayBind(ViewModel, x => x.Categories, x => x.Category.ItemsSource));
                d(this.OneWayBind(ViewModel, x => x.Suppliers, x => x.Supplier.ItemsSource));
                d(this.BindCommand(ViewModel, x => x.CancelSearch, x => x.Cancel));
                d(this.BindCommand(ViewModel, x => x.SearchProduct, x => x.Search, x => x.SearchModel));
                d(this.BindCommand(ViewModel, x => x.SelectProduct, x => x.SelectProduct, x => x.SelectedProduct));
                d(this.WhenAnyValue(x => x.Products.SelectedItem).Subscribe(x => ViewModel.SelectedProduct = (ProductDto)x));
                d(this.WhenAnyValue(x => x.ViewModel.LoadSearchData).SelectMany(x => x.Execute()).Subscribe());
            });
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (ProductSearchViewModel)value;
        }

        public ProductSearchViewModel ViewModel
        {
            get => (ProductSearchViewModel)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductSearchViewModel), typeof(ProductSearchView));
    }
}

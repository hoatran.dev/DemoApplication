﻿using System.Reactive;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using DemoApplication.Domain.Provider;
using DemoApplication.Service;
using DemoApplication.View.Login;
using DemoApplication.View.Product.Dialog;
using DemoApplication.ViewModel.Login;
using DemoApplication.ViewModel.Main;
using DemoApplication.ViewModel.Product.Dialog;
using ReactiveUI;
using Splat;

namespace DemoApplication.View.Main
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : IViewFor<MainViewModel>
    {
        public MainView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                d(this.BindCommand(ViewModel, x => x.Search, view => view.Search));
                d(this.OneWayBind(ViewModel, x => x.Orders, view => view.DataGrid.ItemsSource));
                d(this.ViewModel.SearchProductResult.RegisterHandler(interacion =>
                {
                    var vm = new ProductSearchViewModel(Locator.Current.GetService<IProductService>());
                    var searchDialog = new ProductSearchView(vm);
                    var result = searchDialog.ShowDialog();
                    interacion.SetOutput(searchDialog.SelectedProduct);
                }));
            });
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (MainViewModel)value;
        }

        public MainViewModel ViewModel
        {
            get => (MainViewModel)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(MainViewModel), typeof(MainView), new PropertyMetadata(null));
    }
}

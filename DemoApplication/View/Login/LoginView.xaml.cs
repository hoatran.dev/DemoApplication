﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DemoApplication.Converter;
using DemoApplication.ViewModel.Login;
using ReactiveUI;

namespace DemoApplication.View.Login
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : IViewFor<LoginViewModel>
    {
        public LoginView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberOne, Observable.Return(1)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberTwo, Observable.Return(2)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberThree, Observable.Return(3)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberFour, Observable.Return(4)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberFive, Observable.Return(5)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberSix, Observable.Return(6)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberSeven, Observable.Return(7)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberEight, Observable.Return(8)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberNine, Observable.Return(9)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.NumberZero, Observable.Return(0)));
                d(this.BindCommand(ViewModel, x => x.UserInput, v => v.Clear, Observable.Return(-1)));
                d(this.OneWayBind(ViewModel, x=> x.PinCode, v => v.PinCode.Text,vmToViewConverterOverride: new NumberToPinConverter()));
            });
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (LoginViewModel)value;
        }

        public LoginViewModel ViewModel { get; set; }
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(LoginViewModel), typeof(LoginView), new PropertyMetadata(null));
    }
}

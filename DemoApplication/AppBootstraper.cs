﻿using DemoApplication.Converter;
using DemoApplication.Domain.Database;
using DemoApplication.Domain.Database.Repository;
using DemoApplication.Domain.Provider;
using DemoApplication.Service;
using DemoApplication.View;
using DemoApplication.View.Login;
using DemoApplication.View.Main;
using DemoApplication.View.Product.Dialog;
using DemoApplication.ViewModel;
using DemoApplication.ViewModel.Login;
using DemoApplication.ViewModel.Main;
using DemoApplication.ViewModel.Product.Dialog;
using ReactiveUI;
using Splat;

namespace DemoApplication
{
    public class AppBootstraper : ReactiveObject, IScreen
    {
        public RoutingState Router { get; private set; }
        public AppBootstraper()
        {
            Router = new RoutingState();
            Locator.CurrentMutable.RegisterLazySingleton(() => this, typeof(IScreen));
            Locator.CurrentMutable.RegisterLazySingleton(() => new DbContextFactory(), typeof(IDbContextFactory));
            Locator.CurrentMutable.RegisterLazySingleton(() => new ProductRepository(Locator.Current.GetService<IDbContextFactory>()), typeof(IProductRepository));
            Locator.CurrentMutable.RegisterLazySingleton(() => new ProductProvider(Locator.Current.GetService<IProductRepository>()), typeof(IProductProvier));
            Locator.CurrentMutable.RegisterLazySingleton(() => new ProductService(Locator.Current.GetService<IProductProvier>()), typeof(IProductService));
            Locator.CurrentMutable.RegisterLazySingleton(() => new ShellViewModel(Locator.Current.GetService<IScreen>()), typeof(ShellViewModel));
            Locator.CurrentMutable.RegisterLazySingleton(() => new ShellView(Locator.Current.GetService<ShellViewModel>()), typeof(IViewFor<ShellViewModel>));
            Locator.CurrentMutable.RegisterLazySingleton(() => new MainView(), typeof(IViewFor<MainViewModel>));
            Locator.CurrentMutable.RegisterLazySingleton(() => new LoginView(), typeof(IViewFor<LoginViewModel>));

        }
    }
}
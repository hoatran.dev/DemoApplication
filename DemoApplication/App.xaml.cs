﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using DemoApplication.View;
using DemoApplication.ViewModel;
using ReactiveUI;
using Splat;

namespace DemoApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static AppBootstraper Bootstraper;
        public static ShellView ShellView;
        public App()
        {
            Bootstraper = new AppBootstraper();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Mapper.Initialize(cfg =>
                cfg.AddProfiles(new []
                {
                    "DemoApplication",
                    "DemoApplication.Domain"
                })
            );
            ShellView = (ShellView)Locator.Current.GetService<IViewFor<ShellViewModel>>();
            ShellView.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using ReactiveUI;

namespace DemoApplication.Converter
{
    public class NumberToPinConverter : IBindingTypeConverter
    {
        public int GetAffinityForObjects(Type fromType, Type toType)
        {
            return 100;
        }

        public bool TryConvert(object @from, Type toType, object conversionHint, out object result)
        {
            var resultTemp = "_ _ _ _";
            try
            {
                var pin = from as string;
                if (!string.IsNullOrEmpty(pin))
                {
                    var charPins = new List<char>();
                    var pinLength = pin.Length;
                    for (var i = 0; i < pinLength; i++)
                    {
                        charPins.Add(pin[i]);
                    }
                    while (charPins.Count < 4)
                    {
                        charPins.Add('_');
                    }
                    resultTemp = string.Join(" ", charPins);
                }
            }
            catch (Exception)
            {
                // ignored
            }
            result = resultTemp;
            return true;
        }
    }
}
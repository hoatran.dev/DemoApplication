﻿using AutoMapper;
using DemoApplication.Domain.Model;
using DemoApplication.ViewModel.Product.Dialog;

namespace DemoApplication
{
    public class DemoApplicationMapperProfile : Profile
    {
        public DemoApplicationMapperProfile()
        {
            CreateMap<ProductSearchInfoViewModel, ProductSearchModel>();
        }
    }
}
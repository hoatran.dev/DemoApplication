﻿using System;
using System.Collections.Generic;
using DemoApplication.Domain.Model;
using DemoApplication.ViewModel.Product.Dialog;

namespace DemoApplication.Service
{
    public interface IProductService
    {
        IObservable<List<ProductDto>> SearchProduct(ProductSearchInfoViewModel searchModel);
        IObservable<List<CategoryDto>> GetCategory();
        IObservable<List<SupplierDto>> GetSupplier();
    }
}
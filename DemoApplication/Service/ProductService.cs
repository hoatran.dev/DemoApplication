﻿using System;
using System.Collections.Generic;
using AutoMapper;
using DemoApplication.Domain.Model;
using DemoApplication.Domain.Provider;
using DemoApplication.ViewModel.Product.Dialog;

namespace DemoApplication.Service
{
    public class ProductService : IProductService
    {
        public IProductProvier ProductProvier;
        public ProductService(IProductProvier productProvier)
        {
            ProductProvier = productProvier;
        }

        public IObservable<List<ProductDto>> SearchProduct(ProductSearchInfoViewModel searchModel)
        {
            return ProductProvier.SearchProduct(Mapper.Map<ProductSearchModel>(searchModel));
        }

        public IObservable<List<CategoryDto>> GetCategory()
        {
            return ProductProvier.GetAllCategory();
        }

        public IObservable<List<SupplierDto>> GetSupplier()
        {
            return ProductProvier.GetAllSupplier();
        }
    }
}
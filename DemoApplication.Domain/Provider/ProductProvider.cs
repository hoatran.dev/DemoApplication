﻿using System;
using System.Collections.Generic;
using System.Reactive.Threading.Tasks;
using DemoApplication.Domain.Database.Repository;
using DemoApplication.Domain.Model;

namespace DemoApplication.Domain.Provider
{
    public class ProductProvider : IProductProvier
    {
        private readonly IProductRepository _productRepository;
        public ProductProvider(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public IObservable<List<ProductDto>> SearchProduct(ProductSearchModel searchModel, int maxRowSelect = 50)
        {
            return _productRepository.SearchProduct(searchModel).ToObservable();
        }

        public IObservable<List<CategoryDto>> GetAllCategory()
        {
            return _productRepository.GetAllCategory().ToObservable();
        }

        public IObservable<List<SupplierDto>> GetAllSupplier()
        {
            return _productRepository.GetAllSupplier().ToObservable();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using DemoApplication.Domain.Model;

namespace DemoApplication.Domain.Provider
{
    public interface IProductProvier
    {
        IObservable<List<ProductDto>> SearchProduct(ProductSearchModel searchModel, int maxRowSelect = 50);
        IObservable<List<CategoryDto>> GetAllCategory();
        IObservable<List<SupplierDto>> GetAllSupplier();
    }
}
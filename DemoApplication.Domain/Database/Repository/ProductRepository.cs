﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using DemoApplication.Domain.Model;

namespace DemoApplication.Domain.Database.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly IDbContextFactory _dbContextFactory;
        public ProductRepository(IDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public async Task<List<ProductDto>> SearchProduct(ProductSearchModel searchModel, int maxRowSelect = 50)
        {
            return await Task.Run(() =>
            {
                using (var dbContext = _dbContextFactory.NewDbContext())
                {
                    var productQuery = from product in dbContext.Products
                                       where product.Enable
                                             && (string.IsNullOrEmpty(searchModel.Barcode) || product.Barcode.Contains(searchModel.Barcode))
                                             && (searchModel.Supplier <=0 || searchModel.Supplier == product.ServerId)
                                             && (searchModel.Category <=0 || searchModel.Category == product.CategoryId)
                                             orderby product.LastEdit descending
                                       select new ProductDto
                                       {
                                           Id = product.Id,
                                           Barcode = product.Barcode,
                                           Name = product.Name,
                                           Cost = product.PriceCold,
                                           Selling = product.PriceUnit
                                       };
                    return productQuery.Take(maxRowSelect).ToList();
                }
            });
        }

        public async Task<List<CategoryDto>> GetAllCategory()
        {
            return await Task.Run(() =>
            {
                using (var dbContext = _dbContextFactory.NewDbContext())
                {
                    var categories = from category in dbContext.Categories
                                     where category.Enable
                                     select new CategoryDto
                                     {
                                         Id = category.Id,
                                         Name = category.Name
                                     };
                    return categories.ToList();
                }
            });
        }

        public async Task<List<SupplierDto>> GetAllSupplier()
        {
            return await Task.Run(() =>
            {
                using (var dbContext = _dbContextFactory.NewDbContext())
                {
                    var suppliers = from supplier in dbContext.Suppliers
                                    where supplier.Enable && supplier.ServerId != null
                                    select new SupplierDto
                                    {
                                        Id = supplier.Id,
                                        Name = supplier.Name,
                                        ServerId = supplier.ServerId.Value
                                    };
                    return suppliers.ToList();
                }
            });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DemoApplication.Domain.Model;

namespace DemoApplication.Domain.Database.Repository
{
    public interface IProductRepository
    {
        Task<List<ProductDto>> SearchProduct(ProductSearchModel searchModel, int maxRowSelect = 50);
        Task<List<CategoryDto>> GetAllCategory();
        Task<List<SupplierDto>> GetAllSupplier();
    }
}
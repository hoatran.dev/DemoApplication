﻿using System.Threading.Tasks;

namespace DemoApplication.Domain.Database.Repository
{
    public interface IUserRepository
    {
        Task<bool> ValidateUserPin(string pinCode);
    }
}
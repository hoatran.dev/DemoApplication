﻿namespace DemoApplication.Domain.Database
{
    public interface IDbContextFactory
    {
        EposDbContext NewDbContext();
    }
}
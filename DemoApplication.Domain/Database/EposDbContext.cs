﻿using System.Data.Entity;
using DemoApplication.Domain.Database.DatabaseModel;

namespace DemoApplication.Domain.Database
{
    public class EposDbContext : DbContext
    {
        public EposDbContext() : base(nameOrConnectionString: "EposDomain")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
    }
}
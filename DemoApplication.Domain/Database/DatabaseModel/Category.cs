﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoApplication.Domain.Database.DatabaseModel
{
    [Table("category", Schema = "public")]
    public class Category
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("enable")]
        public bool Enable { get; set; }

    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoApplication.Domain.Database.DatabaseModel
{
    [Table("supplier", Schema = "public")]
    public class Supplier
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("server_id")]
        public int? ServerId { get; set; }
        [Column("enable")]
        public bool Enable { get; set; }
    }
}
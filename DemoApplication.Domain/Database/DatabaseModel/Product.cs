﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoApplication.Domain.Database.DatabaseModel
{
    [Table("product", Schema = "public")]
    public class Product
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("barcode")]
        public string Barcode { get; set; }

        [Column("price_unit")]
        public decimal PriceUnit { get; set; }

        [Column("price_cold")]
        public decimal PriceCold { get; set; }

        [Column("category_id")]
        public int CategoryId { get; set; }

        [Column("server_id")]
        public int ServerId { get; set; }

        [Column("enable")]
        public bool Enable { get; set; }

        [Column("last_edit")]
        public DateTime LastEdit { get; set; }
    }
}
﻿namespace DemoApplication.Domain.Database
{
    public class DbContextFactory : IDbContextFactory
    {
        public EposDbContext NewDbContext()
        {
            return new EposDbContext();
        }
    }
}
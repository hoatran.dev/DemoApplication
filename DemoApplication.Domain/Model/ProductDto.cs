﻿namespace DemoApplication.Domain.Model
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Barcode { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
        public decimal Selling { get; set; }
        public string ProfitMargin { get; set; }
    }
}
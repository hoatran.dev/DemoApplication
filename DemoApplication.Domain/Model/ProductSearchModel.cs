﻿namespace DemoApplication.Domain.Model
{
    public class ProductSearchModel
    {
        public string Barcode;
        public string SupplierCode;
        public string Name;
        public int Supplier;
        public int Category;
    }
}
﻿namespace DemoApplication.Domain.Model
{
    public class CategoryDto
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
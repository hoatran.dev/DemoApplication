﻿namespace DemoApplication.Domain.Model
{
    public class SupplierDto
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int ServerId { get; set; }
    }
}